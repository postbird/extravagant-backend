-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2018-05-03 12:11:22
-- 服务器版本： 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `extravagantbackend`
--

-- --------------------------------------------------------

--
-- 表的结构 `extravagant_admin`
--

CREATE TABLE `extravagant_admin` (
  `aid` int(255) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理员信息表';

--
-- 转存表中的数据 `extravagant_admin`
--

INSERT INTO `extravagant_admin` (`aid`, `name`, `password`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', '$2y$10$A7UGAaerSRF38V83Esxpeeec/zyhlblhURqyka9Ik05UJvMqB/Zgm', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `extravagant_message`
--

CREATE TABLE `extravagant_message` (
  `mid` int(255) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `concat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='信息表';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `extravagant_admin`
--
ALTER TABLE `extravagant_admin`
  ADD PRIMARY KEY (`aid`);

--
-- Indexes for table `extravagant_message`
--
ALTER TABLE `extravagant_message`
  ADD PRIMARY KEY (`mid`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `extravagant_admin`
--
ALTER TABLE `extravagant_admin`
  MODIFY `aid` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `extravagant_message`
--
ALTER TABLE `extravagant_message`
  MODIFY `mid` int(255) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
