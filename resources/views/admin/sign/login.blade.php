<!doctype html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>管理员登陆</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.bootcss.com/toastr.js/latest/toastr.min.css" rel="stylesheet">
    <script src="https://cdn.bootcss.com/jquery/2.0.1/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.bootcss.com/toastr.js/latest/toastr.min.js"></script>
</head>
<body>
@if(session()->get('err_flash_msg'))
    @if(session()->get('err_flash_msg')['flag'])
        <script>toastr.success("{{session()->get('err_flash_msg')['msg']}}")</script>
    @else
        <script>toastr.error("{{session()->get('err_flash_msg')['msg']}}")</script>
    @endif
@endif
<div class="container">
    <div class="row" style="margin-top: 10%;">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body" style="padding:30px;">
                    <h3 class="text-center">管理员登录：</h3>
                    <form action="" class="form" method="post">
                        <div class="form-group">
                            <label for="name">账户：</label>
                            <input type="text"  name="name" class="form-control" id="name" required>
                        </div>
                        <div class="form-group">
                            <label for="password">密码：</label>
                            <input type="password" name="password" class="form-control" id="password" required>
                        </div>
                        <div class="form-group text-right">
                            {{csrf_field()}}
                            <button class="btn btn-primary">登录</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>

    </div>
</div>
</body>
</html>