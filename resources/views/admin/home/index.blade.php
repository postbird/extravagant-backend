@extends('admin.common')

@section('title','首页')

@section('header')

@endsection

@section('main')
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 50px;">
            <h2>历史数据：[ extravagant.polisdesign.cn ]</h2>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered">
                <tr>
                    <th>序号</th>
                    <th>姓名</th>
                    <th>邮箱</th>
                    <th>电话</th>
                    <th>提交时间</th>
                </tr>
                @forelse($list as $item)
                    <tr>
                        <td>{{$item->mid}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->concat}}</td>
                        <td>{{$item->updated_at}}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center">无数据</td>
                    </tr>
                @endforelse
            </table>
        </div>
    </div>
</div>
@endsection

@section('footer')
@endsection