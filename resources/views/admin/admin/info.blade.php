@extends('admin.common')

@section('title','首页')

@section('header')

@endsection

@section('main')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class=" panel panel-default"  style="padding: 20px;">
                    登陆IP: {{$ip}}
                </div>
            </div>
            <div class="col-md-12" style="margin-bottom: 20px;">
                <h2>修改密码</h2>
            </div>
            <div class="col-md-4">
                <form action="{{url('/admin/admin/password')}}" method="post" >
                    <div class="form-group">
                        <label for="">旧密码：</label>
                        <input type="password" name="oldpassword" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">新密码：</label>
                        <input type="password" name="newpassword" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">重复新密码：</label>
                        <input type="password" name="checkpassword" class="form-control">
                    </div>
                    <div class="form-group text-right">
                        {{csrf_field()}}
                        <button class="btn btn-primary">提交</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer')
@endsection