<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
  /**
   * @title index
   * @description 管理员首页
   * @author postbird
   */
  public function index(){
    $list = Message::orderBy('updated_at','desc')->get();
    return view('admin.home.index',['list'=>$list]);
  }

    /**
     * @title adminInfo
     * @description 修改adminPass页面
     * @author  postbird
     */
  public function adminInfo(Request $request){
      $ip = $request->getClientIp();
      return view('admin.admin.info',['ip'=>$ip]);
  }

    /**
     * @title passwordAction
     * @description 修改密码操作
     * @author  postbird
     */
  public function passwordAction(Request $request){
      $old = $request->post('oldpassword');
      $new = trim($request->post('newpassword'));
      $check = $request->post('checkpassword');
      $aid = $request->session()->get('loginInfo')['aid'];
      $info = Admin::find($aid);
      if(!Hash::check($old,$info->password)){
          flash_msg(false,'旧密码错误');
          return back();
      }
      if(strlen($new)===0){
          flash_msg(false,'密码不能为空');
          return back();
      }
      if($new !== $check){
          flash_msg(false,'两次密码不一致');
          return back();
      }
      $info->password = Hash::make($new);
      try{
          if($info->save()){
              flash_msg(true,"修改成功");
          }else{
              flash_msg(false,"修改失败");
          }
          return redirect(url('/admin/'));
      }catch (\Exception $exception){
          flash_msg(false,$exception->getMessage());
          return back();
      }
  }
}
