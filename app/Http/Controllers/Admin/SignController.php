<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class SignController extends Controller
{
  /**
   * @title index
   * @description 登录首页
   * @author postbird
   */
    public function login(){
      return view('admin.sign.login');
    }

  /**
   * @title loginAction
   * @description 登录处理
   * @param Request $request
   */
    public function loginAction(Request $request){
      $name = $request->post('name');
      $password = $request->post('password');
      $info = Admin::where('name',$name)->first();
      if(!$info){
        flash_msg(false,'账户不存在');
        return back();
      }
      if(!Hash::check($password,$info->password)){
        flash_msg(false,'用户名或密码错误');
        return back();
      }
      flash_msg(true,'管理员登陆');
      $request->session()->put('logined',true);
      $request->session()->put('loginInfo',$info);
      return redirect(url('/admin/'));
    }

  /**
   * @title logout
   * @description 退出登录
   * @author postbird
   */
    public function logout(){
      session()->flush();
      flash_msg(true,'退出登录');
      return redirect(url('/admin/sign/in'));
    }
}
