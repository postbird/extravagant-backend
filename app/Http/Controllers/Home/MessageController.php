<?php

namespace App\Http\Controllers\Home;

use App\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
  /**
   * @title add
   * @description 添加信息
   * @author  postbird
   * @param Request $request
   */
    public function add(Request $request){
      $name=trim($request->post('name'));
      $email=trim($request->post('email'));
      $concat=trim($request->post('concat'));
      if(strlen($name) ===0 || strlen($email) ===0  || strlen($concat) ===0 ){
          return response()->json(create_json_return_data(-1,'姓名/邮件/电话必须填写'));
      }
      $Message = new Message();
      $Message->name = $name;
      $Message->email = $email;
      $Message->concat = $concat;
      try{
          if($Message->save()){
              return response()->json(create_json_return_data(0,'提交成功'));
          }else{
              return response()->json(create_json_return_data(-1,'提交失败,请重试'));
          }
      }catch (\Exception $exception){
          return response()->json(create_json_return_data(-1,'发生异常,请重试'));
      }
    }
}
