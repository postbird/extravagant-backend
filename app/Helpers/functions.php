<?php
/**
 * Created by PhpStorm.
 * User: postbird
 * Date: 2018/5/3
 * Time: 16:06
 */

/**
 * @title flash_msg
 * @description 闪存session信息
 * @author  postbird
 *
 * @param bool $flag
 * @param string $msg
 */
function flash_msg($flag=false,$msg=''){
  $data = [
    'flag'=>$flag,
    'msg'=>$msg,
  ];
  session()->flash('err_flash_msg',$data);
}

function create_json_return_data($code=0,$msg='',$data=[]){
  return array(
    'code'=>$code,
    'msg'=>$msg,
    'data'=>$data
  );
}