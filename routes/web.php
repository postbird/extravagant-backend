<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::prefix('/admin')->namespace('Admin')->middleware(['checkAdminLogin'])->group(function(){
  Route::get('/','HomeController@index');
    Route::get('/admin/info','HomeController@adminInfo');
    Route::post('/admin/password','HomeController@passwordAction');
});

Route::prefix('/admin/sign')->namespace('Admin')->group(function(){
  Route::get('/in','SignController@login');
  Route::post('/in','SignController@loginAction');
  Route::any('/out','SignController@logout');

});


